#include <time.h>
#include <pthread.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdatomic.h>
#include "iomu.h"

/*#define IOMU_RING_SIZE 0x1000 //4KB*/
#define IOMU_RING_SIZE 0x100000 //1MB

struct iomu_element {
  atomic_bool ready;
  struct iovec iov;
};

struct iomu_internal {
  pthread_t  thread;
  bool running;
  iomu_element ring[IOMU_RING_SIZE];
  atomic_ulong head, tail, size;
  uint64_t sent;
  uint64_t pushed;
};

static void compute_chunk_size(IOMURing *r)
{
  r->params.chunk = r->spec.throughput*1e-9*r->params.dt / r->params.dd;
  printf("chunk size is %zu\n", r->params.chunk);
}

IOMURing* iomu_create(IOMUParam params, IOMUSpec spec,
    void(*handler)(struct iovec, void *opaque)) 
{
  IOMURing *q = malloc(sizeof(IOMURing));
  q->params = params;
  q->spec = spec;
  q->next = NULL;
  q->_ = malloc(sizeof(iomu_internal));
  q->_->running = false;
  q->_->head = 0;
  q->_->tail = 0;
  q->_->sent = 0;
  q->_->size = 0;
  q->_->pushed = 0;
  q->handler = handler;

  compute_chunk_size(q);
  return q;
}

void iomu_destroy(IOMURing **qp) 
{
  IOMURing *q = *qp;

  /* shut down the queue thread */
  if(q->_->running && pthread_cancel(q->_->thread)) {
    fprintf(stderr, "failed to stop iomu queue thread\n");
  }

  /* free the internals struct */
  free(q->_);

  free(q);
  *qp = NULL;
}

void do_push(IOMURing *q, struct iovec iov) 
{
  atomic_ulong i = q->_->head;
  for(;;) {
    // wait for ring to not be full
    while(q->_->size >= IOMU_RING_SIZE) {
      i = q->_->head;
    }

    /* increment head on condition ring is still not full. we do this by 
     * checking the head has not moved (moving the tail can only make the ring 
     * smaller) */
    if(atomic_compare_exchange_strong(&(q->_->head), &i, (i+1)%IOMU_RING_SIZE)) {
      /* if atomic check passed we have moved the ring forward and now own the 
       * ring index @i */
      break;
    }
  }

  q->_->ring[i].iov = iov;
  q->_->ring[i].ready = true;
  q->_->size++;
  q->_->pushed += iov.iov_len;
}


void iomu_push(IOMURing *q, struct iovec iov) 
{ 
  /* subdivide iovecs that are larger than the chunk size into smaller iovs */
  if(iov.iov_len > q->params.chunk) {
    size_t n = iov.iov_len / q->params.chunk;
    size_t r = iov.iov_len % q->params.chunk;
    char *bp = (char*)iov.iov_base;
    for(size_t i=0; i<n; i++) {
      struct iovec subiov = {
        .iov_base = &bp[i*q->params.chunk],
        .iov_len = q->params.chunk
      };
      do_push(q, subiov);
    }
    if(r) {
      struct iovec subiov = {
        .iov_base = &bp[n*q->params.chunk],
        .iov_len = r
      };
      do_push(q, subiov);
    }
  }
  else {
    do_push(q, iov);
  }
}


size_t iomu_len(IOMURing *q)
{
  return q->_->size;
}

size_t iomu_sent(IOMURing *r)
{
  return r->_->sent;
}

size_t iomu_pushed(IOMURing *r)
{
  return r->_->pushed;
}

static void cycle(IOMURing *q)
{
  /* only one thread moves the tail, so concurrent operations on the tail itself
   * are not a concern. furthermore since the tail is an atomic object any updates
   * made are coherent to observers */

  uint64_t sent = 0,
           limit = q->spec.throughput*1e-9 * q->params.dt;

  while(q->_->size && sent < limit) {
    /* spin wait for the ring element to become ready */
    while(!q->_->ring[q->_->tail].ready) { }

    /* run the handler for the element */
    q->handler(q->_->ring[q->_->tail].iov, q->harg);
    sent += q->_->ring[q->_->tail].iov.iov_len;
    q->_->tail = (q->_->tail + 1) % IOMU_RING_SIZE;
    q->_->size--;
  }
  q->_->sent += sent;
}

static void* churn(void *opaque)
{
  IOMURing *q = opaque;
  for(;;) {

    struct timespec request = {
        .tv_sec = 0,
        .tv_nsec = q->params.dt
    };
    struct timespec remain;

    cycle(q);

    /* TODO: this doesn't account for the time spent in in the cycle */
    clock_nanosleep(
        CLOCK_MONOTONIC,
        0,
        &request,
        &remain
    );

  }

  return NULL;
}


void iomu_run(IOMURing *q)
{
  pthread_create(&(q->_->thread), NULL, churn, q);
  q->_->running = true;
}

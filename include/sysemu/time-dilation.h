/*
 * QEMU System Emulator, time dilation
 *
 * Copyright (c) 2018 Information Sciences Institute
 *
 * Authors:
 *   Ryan Goodfellow    <rgoodfel@isi.edu>
 *
 * This work is licensed under the terms of the GNU GPL, version 2 or later.
 * See the COPYING file in the top-level directory.
 */
#ifndef TIME_DILATION_H
#define TIME_DILATION_H

#include "qom/object.h"
#include "hw/qdev-properties.h"

void set_tdf(MachineState *ms, uint32_t tdf);

#endif
